const purgecss = require("@fullhuman/postcss-purgecss");

module.exports = {
  plugins: [
    purgecss({
      content: ["../web/themes/custom/example_theme/templates/**/*.twig"],
    }),
  ],
};
